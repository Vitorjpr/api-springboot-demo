# API Simples - Spring Boot

# Para Executar Localmente
## Requisitos
Você precisa ter o Maven e o Java 8 instalados e configurados.

## Executar
Entre na pasta 'demo' do projeto, e execute `mvn spring-boot:run`

Para verificar o resultado, vá até `localhost:8080`
